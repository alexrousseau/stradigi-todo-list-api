import express from 'express';
import {HttpStatusCodeEnum} from './definitions/enums/http-status-code.enum';
import {HealthRouter} from './modules/health/health.resolver';
import {storageSrv} from './modules/storages/storage.service';
import {ErrorService} from './modules/error/error.service';
import {ListRouter} from './modules/rest/list/list.resolver';
import * as BodyParser from 'body-parser';
import {ListItemRouter} from './modules/rest/list-item/list-item.resolver';
import {StorageConstantService} from './modules/constants/storage-constant.service';
import {listSrv} from './modules/rest/list/list.service';
import {listItemSrv} from './modules/rest/list-item/list-item.service';

const dotenv = require('dotenv');

console.log('coucou')

export async function ApiSetupInit() {
    dotenv.config();
    let isListening = false;
    let isInitDone = false;

    const api = express();
    const port: number = parseInt(`${process.env.PORT}`, 10) || 3000;

    // setup modules
    try {
        console.log('service init')
        await storageSrv.init(StorageConstantService.getStorageType());
        listSrv.init();
        listItemSrv.init();
        isInitDone = true;
    } catch (e) {
        ErrorService.emitFatalError(`[SERVER] Error during modules init : ${e}`)
        return
    }

    // service healthcheck & ready to verify our service is successfully started and keeps running
    api.use('/ready', (req, res) => {
        return res.status((isListening && isInitDone) ? HttpStatusCodeEnum.OK : HttpStatusCodeEnum.InternalServerError)
            .json((isListening && isInitDone) ? 'ready' : 'not ready')
    });
    api.use('/health', HealthRouter);



    // setup server middleware
    // Parser type JSON pour les payloads en POST
    api.use(BodyParser.json());

    // setup server routes
    api.use('/list', ListRouter, ListItemRouter);

    api.get('/', (req, res) => {
        res.redirect('/list');
    })

    const server = await api.listen(port)

    console.info(`️[TodoListApi]: Server is running at http://localhost:${port}`)
    isListening = true;

    // for UTs purpose
    return {server, api};
}

export const apiRunning = ApiSetupInit();
