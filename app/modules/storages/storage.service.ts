import {StorageTypeEnum} from '../../definitions/enums/index-enum';
import {MongoService} from './mongo/mongo.service';
import {PostgresqlService} from './postgresql/postgresql.service';
import {ErrorService} from '../error/error.service';

export class StorageService {
    private storageInstance!: MongoService | PostgresqlService;

    constructor(
    ) {
    }

    static getInstancesMap() {
        return {
            [StorageTypeEnum.Mongo]: MongoService,
            [StorageTypeEnum.Postgres]: PostgresqlService,
        }
    }

    async init(type: StorageTypeEnum) {
        if(!type) {
            ErrorService.emitFatalError('[StorageService] No storage type defined in your api Config')
            return;
        }

        const storageSrvByType = StorageService.getInstancesMap()[type]

        if(!storageSrvByType) {
            ErrorService.emitFatalError(`[StorageService] No storage service found for type ${type}`);
            return;
        }

        this.storageInstance = new storageSrvByType();
        await this.storageInstance.init();
    }

    getStorageConnectionClient() {
        return this.storageInstance.getConnectionClient()
    }
}

export const storageSrv = new StorageService();
