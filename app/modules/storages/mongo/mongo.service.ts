import {Db, MongoClient} from 'mongodb';
import {ErrorService} from '../../error/error.service';
import {MongoCollectionNameTypesEnum} from '../../../definitions/enums/index-enum';
import {storageSrv} from '../storage.service';

export class MongoService {
    private _mongoClient!: MongoClient;
    private db!: Db;

    static getCollection(type: MongoCollectionNameTypesEnum) {
        return (storageSrv.getStorageConnectionClient() as Db)
            .collection(process.env[type] || '');
    }

    static getConfig() {
        return {
            connectionString: process.env.MONGO_CONNECTION || '',
            dbName: process.env.MONGO_DB_NAME || '',
        }
    }

    static getMongoNoCollectionFoundMsg(type: MongoCollectionNameTypesEnum) {
        return `[MongoService] No collection "${type} found."`
    }

    async init() {
        const config = MongoService.getConfig();
        this._mongoClient = await new MongoClient(config.connectionString, {
            connectTimeoutMS: 30000,
            keepAlive: true
        });

        await this._mongoClient.connect();

        this.db = await this._mongoClient.db(config.dbName);

        if (!this.db) {
            ErrorService.emitFatalError(`MongoService: No db "${config.dbName} found."`)
            return;
        }

        console.info('Mongo DB connection is setup !')
    }

    getConnectionClient() {
        return this.db;
    }
}
