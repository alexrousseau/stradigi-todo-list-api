import {Pool, PoolClient} from 'pg';
import {PostgresqlQueries} from './postgresql.query';

export class PostgresqlService {
    pgClient!: Pool;
    pgPool!: PoolClient;

    static getConfig() {
        return {
            user: process.env.POSTGRE_USER || '',
            host: process.env.POSTGRE_HOST || '',
            database: process.env.POSTGRE_DB || '',
            password: process.env.POSTGRE_PWD || '',
            port: parseInt(process.env.POSTGRE_PORT || '5432', 10),
        }
    }

    async init() {
        this.pgClient = new Pool(PostgresqlService.getConfig())

        this.pgPool = await this.pgClient.connect();

        await this.initTablesIfNotExists()
    }

    async initTablesIfNotExists() {
        await this.pgPool.query(PostgresqlQueries.addGenerateObjectIdFctToPostgres());

        await this.pgPool.query(PostgresqlQueries.createListTableIfNotExist());

        await this.pgPool.query(PostgresqlQueries.createListItemTableIfNotExist());
    }

    getConnectionClient() {
        return this.pgPool
    }
}
