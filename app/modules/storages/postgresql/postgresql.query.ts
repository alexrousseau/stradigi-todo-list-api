// Allow me to be more generic with typing by switching between mongo and postres
// src: https://gist.github.com/jamarparris/6100413
const addGenerateObjectIdFctToPostgres = () => {
    return `
        CREATE OR REPLACE FUNCTION generate_object_id() RETURNS varchar AS $$
            DECLARE
                time_component bigint;
                machine_id bigint := FLOOR(random() * 16777215);
                process_id bigint;
                seq_id bigint := FLOOR(random() * 16777215);
                result varchar:= '';
            BEGIN
                SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp())) INTO time_component;
                SELECT pg_backend_pid() INTO process_id;
        
                result := result || lpad(to_hex(time_component), 8, '0');
                result := result || lpad(to_hex(machine_id), 6, '0');
                result := result || lpad(to_hex(process_id), 4, '0');
                result := result || lpad(to_hex(seq_id), 6, '0');
                RETURN result;
            END;
        $$ LANGUAGE PLPGSQL;`
}

const createListTableIfNotExist = () => {
    return `CREATE TABLE IF NOT EXISTS list (
        _id varchar(24) NOT NULL default generate_object_id(), 
        name varchar(96) NOT NULL, 
        PRIMARY KEY (_id)
    )`
}

const createListItemTableIfNotExist = () => {
    return `CREATE TABLE IF NOT EXISTS listItem (
        id varchar(24) NOT NULL default generate_object_id(),
        list_id varchar(24) NOT NULL, 
        description varchar(256) NOT NULL, 
        checked boolean, 
        PRIMARY KEY (id),
        FOREIGN KEY (list_id) REFERENCES list(_id)
    )`
}

export const PostgresqlQueries = {
    addGenerateObjectIdFctToPostgres,
    createListTableIfNotExist,
    createListItemTableIfNotExist
}
