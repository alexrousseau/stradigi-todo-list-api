import { ObjectId } from 'mongodb';
import {BaseInterfaceService} from '../base/base-interface.service';
import {
    ByIdQueryInterface,
    PostQueryInterface,
} from '../../../definitions/interfaces/rest-queries-interface';
import {
    ItemByIdQueryInterface,
    ItemInterface,
    NewItemBody, PutItemByIdQueryInterface
} from '../../../definitions/interfaces/item.interface';
import {StorageConstantService} from '../../constants/storage-constant.service';
import {StorageTypeEnum} from '../../../definitions/enums/storage-type.enum';
import {ListItemRequestsAbstract} from './list-item-requests/list-item-requests.abstract';
import {ListItemRequestsMongoService} from './list-item-requests/list-item-requests-mongo.service';
import {ListItemRequestsPostgresqlService} from './list-item-requests/list-item-requests-postgresql.service';

export class ListItemService implements BaseInterfaceService<ItemInterface>{
    requestStorageInstance!: ListItemRequestsAbstract;

    constructor() {
    }

    init() {
        const type = StorageConstantService.getStorageType();
        const requestStorageSrvByType = ListItemService.getInstancesMap()[type];

        this.requestStorageInstance = new requestStorageSrvByType();
    }

    static getInstancesMap() {
        return {
            [StorageTypeEnum.Mongo]: ListItemRequestsMongoService,
            [StorageTypeEnum.Postgres]: ListItemRequestsPostgresqlService,
        }
    }

    async create(query: PostQueryInterface<NewItemBody>): Promise<boolean> {
        return await this.requestStorageInstance.addItem(query)
    }

    async getAll(query: ByIdQueryInterface): Promise<ItemInterface[]> {
        return await this.requestStorageInstance.getItemsForListId(query);
    }

    async getById(query: ItemByIdQueryInterface): Promise<ItemInterface> {
        return await this.requestStorageInstance.getItemByIdForListId(query);
    }

    async update(query: PutItemByIdQueryInterface): Promise<boolean> {
        return await this.requestStorageInstance.updateItemByIdForListId(query)
    }

    async delete(query: ItemByIdQueryInterface): Promise<boolean> {
        return await this.requestStorageInstance.deleteItemByIdForListId(query);
    }


}

export const listItemSrv = new ListItemService();
