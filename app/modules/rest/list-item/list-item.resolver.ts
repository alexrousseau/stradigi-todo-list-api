import {Router} from 'express';
import {ErrorService} from '../../error/error.service';
import {HttpStatusCodeEnum} from '../../../definitions/enums/http-status-code.enum';
import { ObjectId } from 'mongodb';
import {ListItemService, listItemSrv} from './list-item.service';
import {ListErrorService} from '../list/list-error/list-error.service';
import {ListItemErrorService} from './list-item-error/list-item-error.service';

const router: Router = Router();

const itemRoute = '/:id/item'
const itemByIdRoute = `${itemRoute}/:itemId`

router.get('/:id/items', async (req, res, next) => {
    try {
        const data = await listItemSrv.getAll({id: new ObjectId(req.params.id)});
        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on get Lists: ${e}`);
    }
})

router.get(itemByIdRoute, async (req, res, next) => {
    try {
        const data = await listItemSrv.getById({
            listId: new ObjectId(req.params.id),
            id: new ObjectId(req.params.itemId)
        });
        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on get List by ID : ${e}`);
    }
})

router.post(itemRoute, async (req, res, next) => {
    try {
        const payload = {
            listId: new ObjectId(req.params.id),
            description: req.body.description
        };
        const requestPayloadChecked = ListItemErrorService.isPostRequestCorrect(payload);
        if (!requestPayloadChecked.isOk) {
            res.status(HttpStatusCodeEnum.BadRequest).json(requestPayloadChecked.errMsg);
            return;
        }

        const data = await listItemSrv.create({payload});

        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on post List: ${e}`);
    }
})

router.put(itemByIdRoute, async (req, res, next) => {
    try {
        const data = await listItemSrv.update({
            id: new ObjectId(req.params.itemId),
            listId: new ObjectId(req.params.id),
            payload: req.body,
        });
        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on post List: ${e}`);
    }
})

router.delete(itemByIdRoute, async (req, res, next) => {
    try {
        const data = await listItemSrv.delete({
            listId: new ObjectId(req.params.id),
            id: new ObjectId(req.params.itemId)
        });

        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on delete List by id: ${e}`);
    }
})

export const ListItemRouter: Router = router;
