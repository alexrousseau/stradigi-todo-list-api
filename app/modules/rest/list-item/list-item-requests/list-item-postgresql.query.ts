const addItemToListId = () => {
    return `INSERT INTO listItem (description, list_id, checked) VALUES ($1, $2, false)`;
}

const getAllItems = () => {
    return `SELECT * FROM listItem`;
}

const getAllItemsForListId = () => {
    return getAllItems() + ' WHERE list_id = $1';
}

const getItemByIdForListId = () => {
    return getAllItemsForListId() + ' AND id = $2';
}

const updateItemFromListId = (keyToUpdate: string) => {
    return `UPDATE listItem SET ${keyToUpdate} = $1 WHERE id = $2 AND list_id = $3`;
}

const deleteItemFromListId = () => {
    return 'DELETE FROM listItem WHERE id = $1 AND list_id = $2';
}

export const ListItemPostgresqlQueries = {
    addItemToListId,
    getAllItemsForListId,
    getItemByIdForListId,
    updateItemFromListId,
    deleteItemFromListId
}
