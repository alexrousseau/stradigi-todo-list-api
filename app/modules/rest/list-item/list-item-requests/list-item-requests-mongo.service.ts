import {ObjectId} from 'mongodb';

import {ListItemRequestsAbstract} from './list-item-requests.abstract';
import {ByIdQueryInterface, PostQueryInterface} from '../../../../definitions/interfaces/rest-queries-interface';
import {
    ItemByIdQueryInterface,
    NewItemBody,
    PutItemByIdQueryInterface
} from '../../../../definitions/interfaces/item.interface';
import {MongoService} from '../../../storages/mongo/mongo.service';
import {MongoCollectionNameTypesEnum} from '../../../../definitions/enums/mongo-collection-name-types.enum';

export class ListItemRequestsMongoService implements ListItemRequestsAbstract {

    async addItem(query: PostQueryInterface<NewItemBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const queryResponse = await collection.updateOne(
                    { _id: query.payload.listId },
                    { $push: { items: { id: new ObjectId(), description: query.payload.description, checked: false } } }
                );

                // TODO: manage check if queryResponse has successfully update my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        })
    }

    async getItemsForListId<T>(query: ByIdQueryInterface): Promise<T[]> {
        return new Promise<T[]>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const doc = await collection.findOne(
                    { _id: query.id }, {projection: { items: 1 }}
                );
                if(!doc) {
                    reject('Not found');
                    return;
                }
                resolve(doc as T[]);
            } catch (e) {
                reject(e)
            }
        })
    }

    async getItemByIdForListId<T>(query: ItemByIdQueryInterface): Promise<T> {
        return new Promise<T>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const doc = await collection.findOne(
                    { _id: query.listId, 'items.id': {$eq: query.id} }, {projection: { items: { $slice: -1 } }}
                );

                console.log('getItemByIdForListId ===> ', doc);

                resolve(doc?.items[0] as any);
            } catch (e) {
                reject(e)
            }
        })
    }

    async updateItemByIdForListId(query: PutItemByIdQueryInterface): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const queryResponse = await collection.updateOne(
                    { _id: query.listId, 'items.id': {$eq: query.id} },
                    { $set: { [`items.$.${query.payload.keyToUpdate}`]: query.payload.valueToUpdate } }
                );

                // TODO: manage check if queryResponse has successfully update my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        })
    }

    async deleteItemByIdForListId(query: ItemByIdQueryInterface): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const queryResponse = await collection.updateOne(
                    { _id: query.listId },
                    { $pull: { items: { id: query.id} } }
                );

                // TODO: manage check if queryResponse has successfully update my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        })
    }

}
