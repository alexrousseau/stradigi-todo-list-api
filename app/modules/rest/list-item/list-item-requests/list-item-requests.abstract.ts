import {ByIdQueryInterface, PostQueryInterface} from '../../../../definitions/interfaces/rest-queries-interface';
import {ItemByIdQueryInterface, PutItemByIdQueryInterface} from '../../../../definitions/interfaces/item.interface';

export abstract class ListItemRequestsAbstract {

    abstract addItem(query: PostQueryInterface): Promise<boolean>;

    abstract getItemsForListId<T>(query: ByIdQueryInterface): Promise<T[]>;

    abstract getItemByIdForListId<T>(query: ItemByIdQueryInterface): Promise<T>;

    abstract updateItemByIdForListId(query: PutItemByIdQueryInterface): Promise<boolean>;

    abstract deleteItemByIdForListId(query: ItemByIdQueryInterface): Promise<boolean>;
}
