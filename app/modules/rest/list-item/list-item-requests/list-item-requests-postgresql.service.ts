import {PoolClient} from 'pg';

import {ListItemRequestsAbstract} from './list-item-requests.abstract';
import {ByIdQueryInterface, PostQueryInterface} from '../../../../definitions/interfaces/rest-queries-interface';
import {
    ItemByIdQueryInterface,
    NewItemBody,
    PutItemByIdQueryInterface
} from '../../../../definitions/interfaces/item.interface';
import {storageSrv} from '../../../storages/storage.service';
import {ListItemPostgresqlQueries} from './list-item-postgresql.query';

export class ListItemRequestsPostgresqlService implements ListItemRequestsAbstract {

    static getStorageCli() {
        return (storageSrv.getStorageConnectionClient() as PoolClient);
    }

    async addItem(query: PostQueryInterface<NewItemBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListItemRequestsPostgresqlService.getStorageCli().query(
                    ListItemPostgresqlQueries.addItemToListId(),
                    [query.payload.description, query.payload.listId.toHexString()]
                );

                // TODO: manage check if queryResponse has successfully create my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async getItemsForListId<T>(query: ByIdQueryInterface): Promise<T[]> {
        return new Promise<T[]>(async (resolve, reject) => {
            try {
                const success = await ListItemRequestsPostgresqlService.getStorageCli().query(
                    ListItemPostgresqlQueries.getAllItemsForListId(),
                    [query.id.toHexString()]
                );

                resolve(success.rows as T[]);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async getItemByIdForListId<T>(query: ItemByIdQueryInterface): Promise<T> {
        return new Promise<T>(async (resolve, reject) => {
            try {
                const success = await ListItemRequestsPostgresqlService.getStorageCli().query(
                    ListItemPostgresqlQueries.getItemByIdForListId(),
                    [query.listId.toHexString(), query.id.toHexString()]
                );

                resolve(success.rows[0] as T);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async updateItemByIdForListId(query: PutItemByIdQueryInterface): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListItemRequestsPostgresqlService.getStorageCli().query(
                    ListItemPostgresqlQueries.updateItemFromListId(query.payload.keyToUpdate),
                    [
                        query.payload.valueToUpdate,
                        query.id.toHexString(),
                        query.listId.toHexString()
                    ]
                );

                // TODO: manage check if queryResponse has successfully create my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async deleteItemByIdForListId(query: ItemByIdQueryInterface): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListItemRequestsPostgresqlService.getStorageCli().query(
                    ListItemPostgresqlQueries.deleteItemFromListId(),
                    [query.id.toHexString(), query.listId.toHexString()]
                );

                // TODO: manage check if queryResponse has successfully delete my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }
}
