import {ListBody} from '../../../../definitions/interfaces/list.interface';
import {RequestCheckedObj} from '../../list/list-error/list-error.service';
import {NewItemBody} from '../../../../definitions/interfaces/item.interface';

export class ListItemErrorService {
    static getMaxListItemDescLength(): number {
        return 96;
    }

    static getNoPayloadError(): RequestCheckedObj {
        return new RequestCheckedObj(
            false,
            `[List Item Resolver] A list item description must be provided or can't be empty`
        )
    }

    static getDescTooLongErr(): RequestCheckedObj {
        return new RequestCheckedObj(
            false,
            `[List Item Resolver] Your list item description exceeded the max description length: ${ListItemErrorService.getMaxListItemDescLength()}`
        )
    }

    static isPostRequestCorrect(payload: NewItemBody): RequestCheckedObj {
        if(!payload || !(payload?.description) || !(payload?.description.length > 0)) {
            return ListItemErrorService.getNoPayloadError();
        }
        if(payload.description.length > ListItemErrorService.getMaxListItemDescLength()) {
            return ListItemErrorService.getDescTooLongErr()
        }
        return new RequestCheckedObj(true, '');
    }
}
