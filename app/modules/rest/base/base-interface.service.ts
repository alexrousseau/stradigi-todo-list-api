import {
    ByIdQueryInterface,
    PostQueryInterface,
    PutQueryInterface
} from '../../../definitions/interfaces/rest-queries-interface';

export interface BaseInterfaceService<E> {
    init(): void;

    create(query: PostQueryInterface): Promise<boolean>;
    delete(query: ByIdQueryInterface): Promise<boolean>;
    getById(query: ByIdQueryInterface): Promise<E>;
    getAll(query?: ByIdQueryInterface): Promise<E[]>;
    update(query: PutQueryInterface): Promise<boolean>;
}
