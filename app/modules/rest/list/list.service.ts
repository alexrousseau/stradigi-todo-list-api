import {BaseInterfaceService} from '../base/base-interface.service';
import {ListBody, ListInterface} from '../../../definitions/interfaces/list.interface';
import {
    ByIdQueryInterface,
    PostQueryInterface,
    PutQueryInterface
} from '../../../definitions/interfaces/rest-queries-interface';
import {StorageTypeEnum} from '../../../definitions/enums/storage-type.enum';
import {ListRequestsAbstract} from './list-requests/list-requests.abstract';
import {StorageConstantService} from '../../constants/storage-constant.service';
import {ListRequestsMongoService} from './list-requests/list-requests-mongo.service';
import {ListRequestsPostgresqlService} from './list-requests/list-requests-postgresql.service';

export class ListService implements BaseInterfaceService<ListInterface>{
    private requestStorageInstance!: ListRequestsAbstract;

    constructor() {
        console.log('ListService ctor')
    }

    init() {
        console.log('ListService init')
        const type = StorageConstantService.getStorageType();
        const requestStorageSrvByType = ListService.getInstancesMap()[type];

        this.requestStorageInstance = new requestStorageSrvByType();
    }

    static getInstancesMap() {
        return {
            [StorageTypeEnum.Mongo]: ListRequestsMongoService,
            [StorageTypeEnum.Postgres]: ListRequestsPostgresqlService,
        }
    }

    async create(query: PostQueryInterface<ListBody>): Promise<boolean> {
        return await this.requestStorageInstance.create(query)
    }

    async getAll(): Promise<ListInterface[]> {
        return await this.requestStorageInstance.getAll();
    }

    async getById(query: ByIdQueryInterface): Promise<ListInterface> {
        return await this.requestStorageInstance.getById(query);
    }

    async update(query: PutQueryInterface<ListBody>): Promise<boolean> {
        return await this.requestStorageInstance.getByIdAndUpdate(query)
    }

    async delete(query: ByIdQueryInterface): Promise<boolean> {
        return await this.requestStorageInstance.deleteById(query);
    }
}

export const listSrv = new ListService();
