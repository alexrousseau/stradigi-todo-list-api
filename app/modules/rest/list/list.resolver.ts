import {Router} from 'express';
import { ObjectId } from 'mongodb';
import {listSrv} from './list.service';
import {ErrorService} from '../../error/error.service';
import {HttpStatusCodeEnum} from '../../../definitions/enums/http-status-code.enum';
import {ListResponseMsgUtils} from './list-response-msg.utils';
import {ListErrorService} from './list-error/list-error.service';

const router: Router = Router();

router.get('/', async (req, res, next) => {
    try {
        const data = await listSrv.getAll();
        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on get Lists: ${e}`);
    }
})

router.get('/:id', async (req, res, next) => {
    try {
        const data = await listSrv.getById({id: new ObjectId(req.params.id)});
        res.status(HttpStatusCodeEnum.OK).send(data);
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on get List by ID : ${e}`);
    }
})

router.post('/', async (req, res, next) => {
    try {
        const payload = {name: req.body.name};
        const requestPayloadChecked = ListErrorService.isPostRequestCorrect(payload);
        if (!requestPayloadChecked.isOk) {
            res.status(HttpStatusCodeEnum.BadRequest).json(requestPayloadChecked.errMsg);
            return;
        }
        const data = await listSrv.create({payload: {name: req.body.name}});
        res.status(HttpStatusCodeEnum.OK).json(ListResponseMsgUtils.getSuccessfulPostMsg(req.body.name));
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on post List: ${e}`);
    }
})

router.put('/:id', async (req, res, next) => {
    try {
        const data = await listSrv.update({id: new ObjectId(req.params.id), payload: {name: req.body.name}});
        res.status(HttpStatusCodeEnum.OK).json(ListResponseMsgUtils.getSuccessfulPutMsg(req.params.id, req.body.name));
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on post List: ${e}`);
    }
})

router.delete('/:id', async (req, res, next) => {
    try {
        const data = await listSrv.delete({id: new ObjectId(req.params.id)});
        res.status(HttpStatusCodeEnum.OK).json(ListResponseMsgUtils.getSuccessfulDeleteMsg(req.params.id));
    } catch(e) {
        next(e);
        ErrorService.emitError(`[ListResolver] Error occurs on delete List by id: ${e}`);
    }
})

export const ListRouter: Router = router;
