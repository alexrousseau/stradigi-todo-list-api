import {
    ByIdQueryInterface, ListBody,
    PostQueryInterface,
    PutQueryInterface
} from '../../../../definitions/interfaces/index-interface';
import {ListRequestsAbstract} from './list-requests.abstract';
import {MongoService} from '../../../storages/mongo/mongo.service';
import {MongoCollectionNameTypesEnum} from '../../../../definitions/enums/index-enum';

export class ListRequestsMongoService implements ListRequestsAbstract {

    async create(query: PostQueryInterface<ListBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(`[MongoService] No collection "${MongoCollectionNameTypesEnum.List} found."`)
                return;
            }

            try {
                const queryResponse = await collection.insertOne({...query.payload, items: []});

                // TODO: manage check if queryResponse has successfully create my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        })
    }

    async getById<T>(query: ByIdQueryInterface): Promise<T> {
        return new Promise<T>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(`[MongoService] No collection "${MongoCollectionNameTypesEnum.List} found."`)
                return;
            }

            try {
                const doc = await collection.findOne({_id: query.id})
                if(!doc) {
                    reject('Not found');
                    return;
                }
                resolve(doc as T);
            } catch (e) {
                reject(e)
            }
        })
    }

    async getAll<T>(): Promise<T[]> {
        return new Promise<T[]>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const docs = await collection.find().toArray()

                if(!docs) {
                    reject('Not found');
                    return;
                }
                resolve(docs as T[]);
            } catch (e) {
                reject(e)
            }
        })
    }

    async getByIdAndUpdate(query: PutQueryInterface<ListBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const queryResponse = await collection.updateOne(
                    { _id: query.id },
                    { $set: { name: query.payload.name } }
                );

                // TODO: manage check if queryResponse has successfully update my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        })
    }

    async deleteById<T>(query: ByIdQueryInterface): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            const collection = MongoService.getCollection(MongoCollectionNameTypesEnum.List);

            if (!collection) {
                reject(MongoService.getMongoNoCollectionFoundMsg(MongoCollectionNameTypesEnum.List))
                return;
            }

            try {
                const queryResponse = await collection.deleteOne({_id: query.id})

                // TODO: manage check if queryResponse has successfully delete my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch (e) {
                reject(e)
            }
        });
    }
}
