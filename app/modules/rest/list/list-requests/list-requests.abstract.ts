import {
    ByIdQueryInterface,
    PostQueryInterface, PutQueryInterface
} from '../../../../definitions/interfaces/rest-queries-interface';
import {Db} from 'mongodb';
import {PoolClient} from 'pg';
import {ListBody} from '../../../../definitions/interfaces/list.interface';

export type StorageCliType = Db | PoolClient;

export abstract class ListRequestsAbstract {
    abstract create(query: PostQueryInterface<ListBody>): Promise<boolean>

    abstract getById<T>(query: ByIdQueryInterface): Promise<T>;

    abstract getAll<T>(): Promise<T[]>;

    abstract getByIdAndUpdate(query: PutQueryInterface<ListBody>): Promise<boolean>;

    abstract deleteById(query: ByIdQueryInterface): Promise<boolean>;
}


