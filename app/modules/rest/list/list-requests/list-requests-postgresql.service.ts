import {
    ByIdQueryInterface,
    PostQueryInterface, PutQueryInterface
} from '../../../../definitions/interfaces/rest-queries-interface';
import {PoolClient} from 'pg';
import {storageSrv} from '../../../storages/storage.service';
import {ListPostgresqlQueries} from './list-postgresql.query';
import {ListBody} from '../../../../definitions/interfaces/list.interface';

export class ListRequestsPostgresqlService {

    static getStorageCli() {
        return (storageSrv.getStorageConnectionClient() as PoolClient);
    }

    async create(query: PostQueryInterface<ListBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListRequestsPostgresqlService.getStorageCli().query(
                    ListPostgresqlQueries.createNewList(),
                    [query.payload.name]
                );

                // TODO: manage check if queryResponse has successfully create my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async getAll<T>(): Promise<T[]> {
        return new Promise<T[]>(async (resolve, reject) => {
            try {
                const success = await ListRequestsPostgresqlService.getStorageCli().query(
                    ListPostgresqlQueries.getAllLists()
                );

                resolve(success.rows as T[]);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async getById<T>(query: ByIdQueryInterface): Promise<T> {
        return new Promise<T>(async (resolve, reject) => {
            try {
                const success = await ListRequestsPostgresqlService.getStorageCli().query(
                    ListPostgresqlQueries.getListById(),
                    [(query.id).toHexString()]
                );

                resolve(success.rows[0] as T);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async getByIdAndUpdate(query: PutQueryInterface<ListBody>): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListRequestsPostgresqlService.getStorageCli().query(
                    ListPostgresqlQueries.updateListName(),
                    [query.payload.name, query.id.toHexString()]
                );

                // TODO: manage check if queryResponse has successfully update my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }

    async deleteById(query: ByIdQueryInterface): Promise<boolean>{
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const queryResponse = await ListRequestsPostgresqlService.getStorageCli().query(
                    ListPostgresqlQueries.deleteListById(),
                    [query.id.toHexString()]
                );

                // TODO: manage check if queryResponse has successfully delete my elmt
                console.log(queryResponse);

                resolve(!!queryResponse);
            } catch(e) {
                reject('[PostgresqlService]' + e)
            }
        })
    }
}
