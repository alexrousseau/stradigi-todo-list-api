const createNewList = () => {
    return `INSERT INTO list (name) VALUES ($1)`;
}

const getAllLists = () => {
    return `SELECT * FROM list`;
}

const getListById = () => {
    return getAllLists() + ' WHERE _id = $1';
}

const updateListName = () => {
    return 'UPDATE list SET name = $1 WHERE _id = $2';
}

const deleteListById = () => {
    return 'DELETE FROM list WHERE _id = $1';
}

export const ListPostgresqlQueries = {
    createNewList,
    getAllLists,
    getListById,
    updateListName,
    deleteListById,
}
