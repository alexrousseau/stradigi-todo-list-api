import {PostQueryInterface} from '../../../../definitions/interfaces/rest-queries-interface';
import {ListBody} from '../../../../definitions/interfaces/list.interface';

export class RequestCheckedObj {
    isOk: boolean;
    errMsg: string

    constructor(isOk: boolean, errMsg: string) {
        this.isOk = isOk;
        this.errMsg = errMsg;
    }
}

export class ListErrorService {
    static getMaxListNameLength(): number {
        return 96;
    }

    static getNoPayloadError(): RequestCheckedObj {
        return new RequestCheckedObj(
            false,
            `[List Resolver] A list name must be provided or can't be empty`
            )
    }

    static getNameTooLongErr(): RequestCheckedObj {
        return new RequestCheckedObj(
            false,
            `[List Resolver] Your list name exceeded the max name length: ${ListErrorService.getMaxListNameLength()}`
        )
    }

    static isPostRequestCorrect(payload: ListBody): RequestCheckedObj {
        if(!payload || !(payload?.name) || !(payload?.name.length > 0)) {
            return ListErrorService.getNoPayloadError();
        }
        if(payload.name.length > ListErrorService.getMaxListNameLength()) {
            return ListErrorService.getNameTooLongErr()
        }
        return new RequestCheckedObj(true, '');
    }
}
