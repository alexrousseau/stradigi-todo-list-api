export class ListResponseMsgUtils {
    static getSuccessfulPostMsg(name: string) {
        return `Your list named ${name} has successfully been created.`
    }

    static getSuccessfulPutMsg(id: string, name: string) {
        return `Your list with id ${id} has successfully been updated with name: ${name}`
    }

    static getSuccessfulDeleteMsg(id: string) {
        return `Your list with id ${id} has successfully been deleted.`
    }
}
