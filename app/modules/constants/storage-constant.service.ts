import {StorageTypeEnum} from '../../definitions/enums/storage-type.enum';

export class StorageConstantService {
    static getStorageType() {
        return process.env.STORAGE_CONNECTION_TYPE_DEFAULT as StorageTypeEnum || StorageTypeEnum.Mongo
    }
}
