import { Router, Response } from 'express';
import {HttpStatusCodeEnum} from '../../definitions/enums/http-status-code.enum';
import {healthSrv} from './health.service';

const router: Router = Router();

router.get('/',async (req: any, res: Response) => {
    const healthCheck = await healthSrv.basicHealthCheck();
    console.log('coucou health ==> ', healthCheck)
    if (healthCheck) {
        res.status(HttpStatusCodeEnum.OK).json('OK');
    } else {
        res.status(HttpStatusCodeEnum.InternalServerError).json('KO');
    }
});

export const HealthRouter: Router = router;
