export class ErrorService {

    static emitFatalError(err: string): void {
        console.error(new Error(err));
        process.exit(1);
    }

    static emitError(err: string): void {
        console.info(new Error(err));
    }

}
