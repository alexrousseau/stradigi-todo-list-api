export enum StorageTypeEnum {
    Mongo = 'mongo',
    Postgres = 'postgresql'
}
