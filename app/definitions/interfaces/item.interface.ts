import { ObjectId } from 'mongodb';
import {ByIdQueryInterface} from './rest-queries-interface';
import {ItemKeyUpdatableEnum} from '../enums/item-key-updatable.enum';

export interface ItemBody {
    description: string;
}

export interface ItemUpdatePayload {
    keyToUpdate: ItemKeyUpdatableEnum;
    valueToUpdate: boolean | string;
}

export interface NewItemBody extends ItemBody {
    listId: ObjectId;
}

export interface ItemInterface extends ItemBody {
    id: ObjectId;
    checked: boolean;
}

export interface ItemByIdQueryInterface extends ByIdQueryInterface {
    listId: ObjectId;
}

export interface PutItemByIdQueryInterface extends ItemByIdQueryInterface {
    payload: ItemUpdatePayload;
}
