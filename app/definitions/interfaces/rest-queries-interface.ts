import { ObjectId } from "mongodb";

export interface ByIdQueryInterface {
    id: ObjectId;
}

export interface PutQueryInterface<payloadType = unknown> extends ByIdQueryInterface {
    payload: payloadType;
}

export interface PostQueryInterface<payloadType = unknown> {
    payload: payloadType;
}
