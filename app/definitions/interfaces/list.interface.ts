import {ObjectId} from 'mongodb';

export interface ListInterface extends ListBody {
    _id: ObjectId;
}

export interface ListBody {
    name: string;
}
