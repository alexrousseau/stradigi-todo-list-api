import {Server} from 'http';
import {Express} from 'express';
import {apiRunning, ApiSetupInit} from '../../app/api-index';
import {agent as request} from 'supertest';
import {expect} from 'chai';
import {closeInMemoryMongodbConnection, runInMemoryMongoDb} from '../utils/in-memory-mongodb.utils';
import {before} from 'mocha';
import {ListResponseMsgUtils} from '../../app/modules/rest/list/list-response-msg.utils';
import {HttpStatusCodeEnum} from '../../app/definitions/enums/http-status-code.enum';
import {ListErrorService} from '../../app/modules/rest/list/list-error/list-error.service';

describe('List resolver', () => {
    let server: Server, api: Express;

    function setupServerInstance(result: any) {
        if (result && result.server) {
            server = result.server
        }
    }

    before(async () => {
        await runInMemoryMongoDb();

        const result = await apiRunning;
        if (result && result.server) {
            await result.server.close();
        }

        const apiRunned = await ApiSetupInit();
        setupServerInstance(apiRunned);
        if (apiRunned) {
            api = apiRunned.api;
        }
    });
    after(async () => {
        if (server) {
            await server.close();
        }
        await closeInMemoryMongodbConnection();
    })

    it('should GET empty /list', async () => {
        // TEST
        const res = await request(api).get('/list');
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.be.empty;
        expect(res.body).to.be.an("array");
    })

    it('should POST /list', async () => {
        // INIT
        const name = 'first list';
        // TEST
        const res = await request(api).post('/list')
            .send({name});
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.equal(ListResponseMsgUtils.getSuccessfulPostMsg(name));
    })

    it('should POST /list again', async () => {
        // INIT
        const name = 'second list';
        // TEST
        const res = await request(api).post('/list')
            .send({name});
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.equal(ListResponseMsgUtils.getSuccessfulPostMsg(name));
    })

    it('should not POST /list if name is empty', async () => {
        // INIT
        const name = '';
        // TEST
        const res = await request(api).post('/list')
            .send({name});
        // ASSERT
        expect(res.status).to.equal(HttpStatusCodeEnum.BadRequest);
        expect(res.body).to.equal(ListErrorService.getNoPayloadError().errMsg);
    })
    it('should not POST /list if name is > 96', async () => {
        // INIT
        const name = 'ZtSvfiCSBSprRLNiDCJGgGKTQhtXxmgWcjMDBfaEfexxTNSPfrhTJQJXntdRVXgzRfNGyyFLmvNqKpSBwcCjuGyPZAHrBYihP'
        // TEST
        const res = await request(api).post('/list')
            .send({name});
        // ASSERT
        expect(res.status).to.equal(HttpStatusCodeEnum.BadRequest);
        expect(res.body).to.equal(ListErrorService.getNameTooLongErr().errMsg);
    })

    it('should GET all /list', async () => {
        // TEST
        const res = await request(api).get('/list');
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).not.to.be.empty;
        expect(res.body.length).to.equal(2);
        expect(res.body[0].name).to.equal('first list');
        expect(res.body[1].name).to.equal('second list');

    })

    it('should GET specific /list/:id', async () => {
        // INIT
        const allListsRes = await request(api).get('/list');
        const id = allListsRes.body[1]._id;
        // TEST
        const res = await request(api).get(`/list/${id}`);
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body.name).to.equal('second list');
    })

    it('should PUT /list/:id', async () => {
        // INIT
        const allListsRes = await request(api).get('/list');
        const id = allListsRes.body[1]._id;
        const name = 'update second list';
        // TEST
        const res = await request(api).put(`/list/${id}`)
            .send({name});
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.equal(ListResponseMsgUtils.getSuccessfulPutMsg(id, name));
    })

    it('should DELETE /list/:id', async () => {
        // INIT
        const allListsRes = await request(api).get('/list');
        const id = allListsRes.body[1]._id;
        // TEST
        const res = await request(api).delete(`/list/${id}`);
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.equal(ListResponseMsgUtils.getSuccessfulDeleteMsg(id));
    })

    it('should DELETE the last /list/:id', async () => {
        // INIT
        const allListsRes = await request(api).get('/list');
        const id = allListsRes.body[0]._id;
        // TEST
        const res = await request(api).delete(`/list/${id}`);
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.equal(ListResponseMsgUtils.getSuccessfulDeleteMsg(id));
    })

    it('should GET empty /list finally', async () => {
        // TEST
        const res = await request(api).get('/list');
        // ASSERT
        expect(res.status).to.equal(200);
        expect(res.body).to.be.empty;
        expect(res.body).to.be.an("array");
    })
})
