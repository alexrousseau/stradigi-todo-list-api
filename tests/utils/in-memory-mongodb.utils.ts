import {MongoMemoryServer} from 'mongodb-memory-server';
import {StorageTypeEnum} from '../../app/definitions/enums/storage-type.enum';

let mongod: MongoMemoryServer;
let originalMongoName: string;
let originalMongoUri: string;
let originalStorageConnectionTypeDefault: string;

export const runInMemoryMongoDb = async (): Promise<void> => {
    mongod = await MongoMemoryServer.create();
    originalMongoUri = process.env.MONGO_CONNECTION as string;
    originalMongoName = process.env.MONGO_DB_NAME as string;
    originalStorageConnectionTypeDefault = process.env.STORAGE_CONNECTION_TYPE_DEFAULT as string;
    process.env.MONGO_CONNECTION = await mongod.getUri();
    process.env.MONGO_DB_NAME = 'test-stradigi-todo-list-api';
    process.env.STORAGE_CONNECTION_TYPE_DEFAULT = StorageTypeEnum.Mongo;
};

export const closeInMemoryMongodbConnection = async (): Promise<void> => {
    if (mongod) {
        await mongod.stop();
    }
    if (originalMongoUri) {
        process.env.MONGO_CONNECTION = originalMongoUri;
    }
    if (originalMongoName) {
        process.env.MONGO_DB_NAME = originalMongoName;
    }
    if (originalStorageConnectionTypeDefault) {
        process.env.STORAGE_CONNECTION_TYPE_DEFAULT = originalStorageConnectionTypeDefault;
    }
};
