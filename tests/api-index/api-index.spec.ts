import { expect } from "chai";
import {SinonSpy, spy} from 'sinon';
import {ApiSetupInit, apiRunning} from '../../app/api-index';
import {agent as request} from 'supertest';
import {listSrv} from '../../app/modules/rest/list/list.service';
import {storageSrv} from '../../app/modules/storages/storage.service';
import {listItemSrv} from '../../app/modules/rest/list-item/list-item.service';
import {StorageTypeEnum} from '../../app/definitions/enums/storage-type.enum';
import {Express} from 'express';
import {Server} from 'http';


describe('Api Index', () => {
    let server: Server

    function setupServerInstance(result: any) {
        if(result && result.server) {
            server = result.server
        }
    }

    beforeEach(async () => {
        const result = await apiRunning;
        if(result && result.server) {
            await result.server.close();
        }
    })

    afterEach(async () => {
        if(server) {
            await server.close();
        }
    })

    describe('Api init', () => {
        describe('Modules initialisation', () => {
            it('should init storage, list, list item service', async () => {
                const storageSrvInitSpy: SinonSpy = spy(storageSrv, 'init');
                const listSrvInitSpy: SinonSpy = spy(listSrv, 'init');
                const listItemSrvInitSpy: SinonSpy = spy(listItemSrv, 'init');

                const result = await ApiSetupInit();
                setupServerInstance(result);

                expect(storageSrvInitSpy.calledOnce).to.be.true
                expect(storageSrvInitSpy.firstCall.args[0]).to.equal(StorageTypeEnum.Mongo)
                expect(listSrvInitSpy.calledOnce).to.be.true
                expect(listItemSrvInitSpy.calledOnce).to.be.true
            })
        })
    })

    describe('basic api routes', () => {
        let api: Express;
        beforeEach(async () => {
            const apiRunned = await ApiSetupInit();
            setupServerInstance(apiRunned);
            if(apiRunned) {
                api = apiRunned.api;
            }
        })

        it('should GET /health', async () => {
            const res = await request(api).get('/health');
            expect(res.status).to.equal(200);
            expect(res.body).to.equal("OK");
        })

        it('should GET /ready', async () => {
            const res = await request(api).get('/ready');
            expect(res.status).to.equal(200);
            expect(res.body).not.to.be.empty;
            expect(res.body).to.equal("ready");
        })
    })
});


