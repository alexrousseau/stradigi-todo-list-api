# Stradigi Todo List
Technical backend test for Stradigi. 
TODO List management service with RESTful API.

## Start
First, you need to have a .env file with these fields :

| Key | Example value | Description |
| --- | ------------ | ----------- |
| MONGO_CONNECTION | 'mongodb://localhost:27017' | connection string to access mongo data base |
| MONGO_DB_NAME | 'stradigi-todo-list' | name of the data base in mongo  |
| MONGO_TABLE_LIST_NAME | 'List' | The value you gave to the List table in Mongo |
| POSTGRE_USER | 'stradigi' | postgresql username to access data base |
| POSTGRE_PWD | 'stradigi-pwd' | postgresql password to access data base |
| POSTGRE_HOST | 'localhost' | postgresql host name |
| POSTGRE_DB | 'stradigitodolist' | name of the data base in postgresql |
| POSTGRE_PORT | 5432 | postgresql port number |
| STORAGE_CONNECTION_TYPE_DEFAULT | 'mongo' | storage you wish to access |

Then, make sure you have [mongodb](https://docs.mongodb.com/manual/installation/) and/or 
[postgresql](https://www.postgresql.org/download/) installed on your device.

Once data base(s) set up, install the project dependencies : `npm i`.

Finally, you can launch the api using `npm run start:dev`. The api will then be available at `http://localhost:3000`.

You will be able to find some api usage example in this [postman file](./postman/stradigi_test_postman_api.postman_collection.json).

## Tests

To run tests use `npm test`.

## Build

Once the api build with `npm run build`, you can run the builded api with `npm start`.

## Roadmap
A lot to do can be (and must be) added:

- Errors Management: For now I just managed Post request errors for List & List Item resolver but a lot is missing
    - Manage check request payload for List PUT route
    - Manage check request payload for List Item PUT route
    - Manage the response of data base request to return the correct boolean and make sure client is well informed
    of the success or not of its request for:
        - List POST / PUT / DELETE request with mongodb
        - List POST / PUT / DELETE request with postgresql
        - List item POST / PUT / DELETE request with mongodb
        - List item POST / PUT / DELETE request with postgresql
    - Add list-item-response-msg.utils class to list item to manage generic response message.
    - Remove the console.log once done
    
- Add a git hook to run tests before commit/push
    
- Format api response between postgresql config and mongo config. Today with mongo, when you request all the list, it 
returns items 
     
- Tests: A lot of tests are missing, today, only some parts of the api init is tested and the List resolver is tested 
for Mongo config. Tests should be added to :
    - list.resolver with postgresql config
    - list-item.resolver with mongo config
    - list-item.resolver with postgresql config
    - list-error.service
    - list-item-error.service
    - storage.service
    - mongo.service
    - postgresql.service
    - list-response-msg.utils
    

- Typings: I think there is still some any left and typings to simplify in some parts

- Docker: it would have been nice to set up a docker configuration, and a script to run a whole setup of the project 
without having to download mongo, postgresql and setup the databases, collections etc... .

## Feedbacks & thoughts
This technical test was pretty interesting. It has been years since I didn't setup a nodejs/express api without framework.
I felt pretty rusty at first, but a lot came back to me. Concerning the file and directory naming, I kept the standard we 
use in my company project for our nestjs projects, but I understand that .service or .resolver file would not make 
sense for nodejs/express project where I remember to use .router and .module files.

Also, I spent a lot of time figuring what would be the best strategy to switch between data bases configurations. But, 
even if that problematic was new to me I enjoyed discovering Postgresql through this project. I heard about it 
without testing it since the teams I work with mainly manage mongodb databases. It was fun to do SQL again.

If I had to do this test again, I would adopt more of a TDD approach, but I spent too much time figuring out how I would
switch between mongo and postgresql configuration and make generic services and types to develop the project. I think I
tried too much to be generic and include too much complexity at first, so I had to review my services some times. 
